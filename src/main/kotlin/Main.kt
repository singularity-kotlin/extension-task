fun main() {
    val printer =  {x: Int, y: Int  -> println(x.power(y))}
    printer(2,3)
    println(2.power(3))
    2.displayTypeString()
    "hello".displayTypeString()
}