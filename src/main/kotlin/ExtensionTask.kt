import kotlin.math.pow



fun Int.power(exponent: Int): Int {
    return this.toDouble().pow(exponent.toDouble()).toInt();
}

fun <T> T.displayTypeString(): Unit {
    when(this) {
        is Int -> println("Integer")
        is String -> println("String")
        else -> println("not found")
    }
}